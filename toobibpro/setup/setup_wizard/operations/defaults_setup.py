# Copyright (c) 2018, DOKOS and Contributors
# See license.txt

import frappe


def set_website_settings():
	website_settings = frappe.get_single("Website Settings")
	website_settings.app_name = "ToobibPro"
	website_settings.app_logo = "/assets/toobibpro/images/logo_toobib.png"
	website_settings.website_theme = "ToobibPro"
	website_settings.save()

	system_settings = frappe.get_single("System Settings")
	system_settings.float_precision = 2
	system_settings.save()

	frappe.db.commit()