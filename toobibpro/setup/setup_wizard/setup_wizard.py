# Copyright (c) 2019, DOKOS and Contributors
# See license.txt


from .operations import defaults_setup, install_fixtures, toobibpro_setup


def setup_complete(args):
	toobibpro_setup.install_chart_of_accounts()
	fixtures_stage(args)

	setup_defaults()
	setup_toobibpro()


def fixtures_stage(args):
	install_fixtures.install(args.country)


def setup_defaults():
	defaults_setup.set_website_settings()


def setup_toobibpro():
	install_fixtures.codifications()
	toobibpro_setup.add_fiscal_years()
	toobibpro_setup.add_meal_expense_deductions()
	toobibpro_setup.set_default_print_formats()
	toobibpro_setup.make_web_page()
	toobibpro_setup.web_portal_settings()
	toobibpro_setup.disable_signup()
	toobibpro_setup.disable_guest_access()
