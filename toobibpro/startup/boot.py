# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# See license.txt"


import frappe
from frappe.utils import now

from toobibpro.accounting.utils import get_fiscal_year


def boot_session(bootinfo):
	"""boot session - send website info if guest"""

	bootinfo.website_settings = frappe.get_doc("Website Settings")

	if frappe.session.user != "Guest" and bootinfo["home_page"] != "setup-wizard":

		bootinfo.practitioner = frappe.db.get_value(
			"Professional Information Card", dict(user=frappe.session.user), "name"
		)
		bootinfo.fiscal_year = get_fiscal_year(date=now(), practitioner=bootinfo.practitioner)
