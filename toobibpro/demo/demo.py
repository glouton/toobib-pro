# Copyright (c) 2019, Dokos and Contributors
# See license.txt


import frappe

from toobibpro.demo.generate import generate
from toobibpro.demo.setup import ToobibProSetup


def make(days=365, main_password="demotoobibpro"):
	set_flags()
	setup_wizard(main_password)
	frappe.db.commit()

	site = frappe.local.site
	frappe.destroy()
	frappe.init(site)
	frappe.connect()

	set_flags()
	ToobibProSetup().run()
	frappe.db.commit()
	frappe.clear_cache()

	resume(days)


def resume(days):
	set_flags()
	generate(days=days)


def set_flags():
	frappe.flags.mute_emails = True
	frappe.flags.mute_gateways = True
	frappe.flags.mute_notifications = True
	frappe.flags.in_demo = 1


def setup_wizard(main_password="demotoobibpro"):
	print("Setup Wizard...")
	from frappe.desk.page.setup_wizard.setup_wizard import setup_complete

	completed = setup_complete(
		{
			"full_name": "Lucile Asselin",
			"email": "lucile@toobib.org",
			"password": main_password,
			"currency": "EUR",
			"timezone": "Europe/Paris",
			"country": "France",
			"language": "Français",
		}
	)
	print("Setup Wizard Completed", completed)
