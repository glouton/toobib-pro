frappe.ready(() => {
	$(".hero-content .btn-primary").on("click", (e) => {
		e.preventDefault();
		accueillogin("lucile@toobib.org", "demotoobibpro", "/app");
	});

	$(".hero-content .btn-primary-light").on("click", (e) => {
		e.preventDefault();
		accueillogin("julie@toobib.org", "demotoobibpro", "/app");
	});

	$(".navbar-nav .nav-link:contains('Rendez-vous')").on("click", (e) => {
		e.preventDefault();
		accueillogin("isabelle@toobib.org", "demotoobibpro", "/me");
	});

	$(".for-login .email-field #login_email").val("isabelle@toobib.org");
	$(".for-login .password-field #login_password").val("demotoobibpro");

	$(".for-login").append(
		"<div class='text-center sign-up-message'>Pour vous connecter aux espaces de travail de ToobibPro, utilisez l'adresse <strong>lucile@toobib.org</strong> ou <strong>julie@toobib.org</strong></div>"
	);
});

const accueillogin = (username, password, target) => {
	frappe
		.call({
			method: "login",
			args: {
				usr: username,
				pwd: password,
			},
		})
		.then((r) => {
			if (r.exc) {
				alert(__("Error, please contact help@dokos.cloud"));
			} else {
				console.log("Logged in");
				window.location.href = target;
			}
		});
};
