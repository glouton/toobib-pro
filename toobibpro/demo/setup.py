# Copyright (c) 2023, Dokos and Contributors
# See license.txt

import frappe

DEFAULT_SCHEDULE = [
	{"day": "Monday", "start_time": "09:00", "end_time": "12:00"},
	{"day": "Monday", "start_time": "13:00", "end_time": "18:00"},
	{"day": "Tuesday", "start_time": "09:00", "end_time": "12:00"},
	{"day": "Tuesday", "start_time": "13:00", "end_time": "18:00"},
	{"day": "Thursday", "start_time": "09:00", "end_time": "12:00"},
	{"day": "Thursday", "start_time": "13:00", "end_time": "18:00"},
	{"day": "Friday", "start_time": "09:00", "end_time": "12:00"},
	{"day": "Friday", "start_time": "13:00", "end_time": "18:00"},
]


class ToobibProSetup:
	def run(self):
		self.setup_users()
		self.setup_web_page()
		self.setup_website_settings()
		self.setup_theme_scss()
		self.setup_theme_script()
		self.setup_insights()

	def setup_users(self):
		print("Setup Users...")
		from frappe.utils.password import update_password

		lucile = frappe.get_doc("User", "lucile@toobib.org")
		lucile.language = "fr"
		lucile.user_image = "/assets/toobibpro/images/demo/lucile.jpg"
		lucile.add_roles("Midwife", "Patient", "Midwife Substitute", "Appointment User", "Insights User")
		lucile.save()

		pic = frappe.new_doc("Professional Information Card")
		pic.email = "lucile@toobib.org"
		pic.last_name = "Lucile"
		pic.first_name = "Asselin"
		pic.user = "lucile@toobib.org"
		pic.allow_online_booking = 1
		for schedule in DEFAULT_SCHEDULE:
			pic.append("consulting_schedule", schedule)
		pic.insert()

		patient = frappe.get_doc(
			{
				"email": "isabelle@toobib.org",
				"first_name": "Isabelle",
				"last_name": "Dupuy",
				"language": "fr",
				"doctype": "User",
				"send_welcome_email": 0,
				"enabled": 1,
				"user_image": "/assets/toobibpro/images/demo/isabelle.jpg",
			}
		)
		patient.flags.create_contact_immediately = True
		patient.user_type = "Website User"
		patient.insert(ignore_if_duplicate=True)

		patient.reload()
		patient.add_roles("Patient")
		update_password(patient.name, "demotoobibpro")

		physician = frappe.get_doc(
			{
				"email": "julie@toobib.org",
				"first_name": "Julie",
				"last_name": "Gareau",
				"language": "fr",
				"doctype": "User",
				"send_welcome_email": 0,
				"enabled": 1,
				"user_image": "/assets/toobibpro/images/demo/julie.jpg",
			}
		)
		physician.flags.create_contact_immediately = True
		physician.user_type = "System User"
		physician.insert(ignore_if_duplicate=True)

		physician.reload()
		physician.add_roles("Physician", "Patient", "Appointment User", "Insights User")
		update_password(physician.name, "demotoobibpro")

		physician_pic = frappe.new_doc("Professional Information Card")
		physician_pic.email = "julie@toobib.org"
		physician_pic.last_name = "Julie"
		physician_pic.first_name = "Gareau"
		physician_pic.user = "julie@toobib.org"
		physician_pic.allow_online_booking = 1
		for schedule in DEFAULT_SCHEDULE:
			physician_pic.append("consulting_schedule", schedule)
		physician_pic.insert()

	def setup_web_page(self):
		with open(frappe.get_app_path("toobibpro", "demo", "data", "demo_web_page.json")) as f:
			docs = frappe.parse_json(f.read())

			for doc in docs:
				frappe.get_doc(doc).insert()

	def setup_website_settings(self):
		print("Website Settings Setup...")
		website_settings = frappe.get_single("Website Settings")
		website_settings.navbar_search = 0
		website_settings.hide_footer_signup = 1
		website_settings.hide_login = 1
		website_settings.banner_image = "/images/logo_toobib.png"
		website_settings.brand_html = (
			"<img style='width: 40px;' src='/assets/toobibpro/images/logo_toobib.png'>"
		)
		website_settings.copyright = "Association Toobib"
		website_settings.home_page = "demo"
		website_settings.website_theme = "ToobibPro"
		website_settings.address = '<div class="ql-editor read-mode"><p>Démo ToobibPro &middot; ToobibPro est un logiciel libre de gestion de cabinet médical. Le projet est disponible ici : <a href="https://framagit.org/toobib/toobib-pro">Framagit</a></p><p>Crédits photos: Unsplash.com</p></div>'
		website_settings.banner_html = """
		<div class="text-center text-white" style="background-color: #4081ff;">
			<div><a href="https://interhop.org/2023/06/06/fabrique-des-santes" class="font-weight-bold text-white" target="_blank">Cliquez ici pour en savoir plus sur ToobibPro</a></div>
		</div>
		"""
		website_settings.save()

	def setup_theme_scss(self):
		theme = frappe.get_doc("Website Theme", "ToobibPro")
		custom_scss = f"{theme.custom_scss}\n.sidebar-column .navbar-brand {{background-color: white;border-radius: 50px;padding: 2px;}}"
		frappe.db.set_value("Website Theme", "ToobibPro", "custom_scss", custom_scss)

	def setup_theme_script(self):
		with open(frappe.get_app_path("toobibpro", "demo", "data", "demo_web_script.js")) as f:
			script = f.read()
			frappe.db.set_value("Website Theme", "ToobibPro", "js", script)


	def setup_insights(self):
		from frappe.core.doctype.data_import.data_import import import_doc
		for fname in ["insights_query.json", "insights_dashboard.json"]:
			file_path = frappe.get_app_path("toobibpro", "demo", "data", fname)
			import_doc(file_path)