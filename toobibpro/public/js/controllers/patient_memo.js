// Ce module est activé automatiquement au chargement du bureau.
// Dès qu'un formulaire est chargé, il vérifie si le formulaire dispose d'un champ "patient_record".
// Si le champ existe, et qu'il n'est pas vide, un mémo est affiché dans le formulaire.
// Sinon, le mémo est masqué ou supprimé.

import { MemoPanel } from "../toobibpro/memo/memo";

export class PatientMemo extends MemoPanel {
	/** @protected */
	fetch_data() {
		const query_method = "toobibpro.patients.doctype.patient_record.dashboard.patient_dashboard.get_memo";
		return frappe.xcall(query_method, this.query_params);
	}

	/** @protected */
	set_query(/** @type {string} */ patient_record) {
		this.query_params = { patient_record };
	}

	show_on_form(frm, wrapper) {
		let patient_record = frm?.doc?.patient_record;
		if (frm?.doctype === "Patient Record") {
			patient_record = frm.doc?.name;
		}
		if (!patient_record) {
			this.set_state("empty");
			return;
		}

		if (wrapper === this.wrapper) {
			this.set_query(patient_record);
			this.render();
		} else {
			this.set_query(patient_record);
			this.switch_wrapper(wrapper);
		}

		this.set_open_state_from_session();
	}

	set_state(state) {
		super.set_state(state);
		if (state === "open" || state === "closed") {
			// On ne persiste que les états "open" et "closed" car non-transitoires.
			sessionStorage.setItem("patient_memo_state", state);
		}
	}

	get default_open_state () {
		return "open";
	}

	set_open_state_from_session() {
		const state = sessionStorage.getItem("patient_memo_state") || this.default_open_state;
		if (state === "open") {
			this.set_state("open");
		} else {
			this.set_state("closed");
		}
	}
}

frappe.provide("toobibpro");

toobibpro.show_form_memo = (frm) => {
	if (!frm) {
		toobibpro.cur_patient_memo?.refresh();
		return;
	}

	frappe.require("patient-dashboard.bundle.js", () => {
		const wrapper = $(frm.page.wrapper).find(".layout-main")[0];

		if (!toobibpro.cur_patient_memo) {
			toobibpro.cur_patient_memo = new PatientMemo({ wrapper });
		}
		toobibpro.cur_patient_memo.show_on_form(frm, wrapper);
	});
};

$(document).on("form-load", (_event, frm) => {
	toobibpro.show_form_memo(frm);

	// Listen to changes in the patient_record field
	const meta = frappe.get_meta(frm.doctype);
	if (meta.fields.find((f) => f.fieldname === "patient_record")) {
		frappe.model.on(frm.doctype, "patient_record", async () => {
			setTimeout(() => toobibpro.show_form_memo(frm), 0);
		});
	}
});

frappe.router.on("change", () => {
	toobibpro.show_form_memo(cur_frm);
});
