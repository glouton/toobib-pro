// Copyright (c) 2017, DOKOS and Contributors
// See license.txt

// add toolbar icon

// TODO: Move to Navbar settings
$(document).bind("toolbar_setup", function () {
	frappe.app.name = "ToobibPro";

	$('[data-link="docs"]').attr("href", "https://doc.toobib.org");
	$('[data-link="issues"]').attr(
		"href",
		"https://framagit.org/toobib/toobib-pro/-/issues"
	);

	var $help_menu = $(".dropdown-help ul .documentation-links");
	$help_menu.prev().remove();
	$help_menu.prev().remove();
	$help_menu.prev().remove();

	$(
		'<li><a href="mailto:help@toobib.org" \
		target="">' +
			__("Report an Issue") +
			"</a></li>"
	).insertBefore($help_menu);
	$(
		'<li><a href="https://doc.toobib.org" \
		target="_blank">' +
			__("Read the documentation") +
			"</a></li>"
	).insertBefore($help_menu);
	$(
		'<li><a href="https://www.cnil.fr/fr/traitement-de-donnees-de-sante-comment-informer-les-personnes-concernees" \
		target="_blank">' +
			__("Affichette CNIL") +
			"</a></li>"
	).insertBefore($help_menu);
	$(
		'<li><a href="https://toobib.org/files/MODELE_DE_NOTE_D%E2%80%99INFORMATION_DES_USAGERS_RELATIVE_A_L%E2%80%99HEBERGEMENT_DE_DONNEES_DE_SANTE_A_CARACTERE_PERSONNEL.pdf" \
		target="_blank">' +
			__("Information Légale") +
			"</a></li>"
	).insertBefore($help_menu);

	add_demo_bar()
});


const add_demo_bar = () => {
	if (localStorage.getItem('show_demo_message') !== "false") {
		const alert = frappe.show_alert({
			indicator: "orange",
			message: __("All functionalities in this site are in beta stage. For any suggestion/remark/bug report, please open an issue on <a href='https://framagit.org/toobib/toobib-pro/-/issues' style='text-decoration: underline;'>Framagit</a>.")
		}, 9999)

		alert.find('.close, button').click(function() {
			localStorage.setItem("show_demo_message", false);
		});
	}
}
