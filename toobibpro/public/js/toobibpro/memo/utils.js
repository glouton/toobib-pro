export async function saveToMemoSource(source, value) {
	const { doctype, name, fieldname } = source;
	if (!doctype || !name || !fieldname) throw new Error('saveToMemoSource: Invalid MemoSource object');
	return await frappe.db.set_value(doctype, name, fieldname, value);
}

export function isImportant(item) {
	return Boolean(item?.appearance?.important);
}
