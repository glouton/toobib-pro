// Copyright (c) 2020,DOKOS and Contributors
// See license.txt

import { Calendar } from "@fullcalendar/core";
import timeGridPlugin from "@fullcalendar/timegrid";
import listPlugin from "@fullcalendar/list";
import interactionPlugin from "@fullcalendar/interaction";
import dayGridPlugin from "@fullcalendar/daygrid";

frappe.provide("toobibpro.appointment");
frappe.provide("toobibpro.appointment_update");

toobibpro.appointment.AppointmentSelector = class AppointmentSelector {
	constructor(opts) {
		Object.assign(this, opts);
		this.wrapper = this.parent.find("#calendar");
		this.practitioner_selector = $('<div id="practitioner-selector"></div>').appendTo(
			$(this.wrapper)
		);
		this.event_selector = $('<div id="event-selector"></div>').appendTo(
			$(this.wrapper)
		);
		this.event_confirmation = $(
			'<div id="event-confirmation"></div>'
		).appendTo($(this.wrapper));
		this.next_date_selector = $(
			'<div id="next-date-selector"></div>'
		).appendTo($(this.wrapper));
		this.practitioners = [];
		this.selected_practitioner = null;
		this.selected_appointment_category = null;
		this.selected_appointment_type = null;
		this.make();
	}

	make() {
		frappe.utils.make_event_emitter(toobibpro.appointment_update);
		this.build_calendar();
	}

	getPractitioners() {
		return frappe
			.call({
				method: "toobibpro.templates.pages.appointment.get_practitioners_and_appointment_types",
			})
			.then((r) => {
				this.practitioners = r.message;

				if (r.message.length === 1) {
					this.selected_practitioner = r.message[0].name;
				}
			});
	}

	build_calendar() {
		this.getPractitioners().then(() => {
			this.top_section = new toobibpro.appointment.PractitionerSelector({
				parent: this,
			});
			this.calendar_section =
				new toobibpro.appointment.AppointmentCalendar({ parent: this });
		});
	}

	is_group_appointment() {
		return this.selected_appointment_type
			? this.practitioners
					.filter((f) => f.name === this.selected_practitioner)
					.reduce(
						(acc, val) => acc.concat(val.appointment_types),
						[]
					)[0]
					?.[this.selected_appointment_category]?.filter(
						(f) => f.name === this.selected_appointment_type
					)[0]?.group_appointment
			: null;
	}
};

toobibpro.appointment.PractitionerSelector = class PractitionerSelector {
	constructor(opts) {
		Object.assign(this, opts);
		this.practitioners = this.parent.practitioners;
		this.render();
	}

	render() {
		const selectorEl = this.parent.practitioner_selector;
		this.form = new frappe.ui.FieldGroup({
			fields: [
				{
					label: __("Practitioner"),
					fieldname: "practitioner",
					fieldtype: "Select",
					options: this.practitioners.map((f) => f.name),
					change: () => {
						this.parent.selected_practitioner =
							this.form.get_value("practitioner");
						this.form.set_df_property("category", "hidden", 1);
						this.form.set_df_property(
							"appointment_type",
							"hidden",
							1
						);
						this.set_categories();
						toobibpro.appointment_update.trigger("update");
					},
				},
				{
					fieldtype: "Column Break",
					fieldname: "category_column",
				},
				{
					label: __("Category"),
					fieldname: "category",
					fieldtype: "Select",
					options: [],
					hidden: 1,
					change: () => {
						this.parent.selected_appointment_category =
							this.form.get_value("category");
						this.set_appointment_types();
						toobibpro.appointment_update.trigger("update");
					},
				},
				{
					fieldtype: "Column Break",
				},
				{
					label: __("Appointment Type"),
					fieldname: "appointment_type",
					fieldtype: "Select",
					options: [],
					hidden: 1,
					change: () => {
						this.parent.selected_appointment_type =
							this.form.get_value("appointment_type");
						this.set_description();
						toobibpro.appointment_update.trigger("update");
					},
				},
				{
					fieldtype: "Section Break",
				},
				{
					fieldname: "description",
					fieldtype: "HTML",
					hidden: 1,
				},
			],
			body: selectorEl[0],
		});
		this.form.make();

		if (this.practitioners.length === 1) {
			this.form.set_value("practitioner", this.practitioners[0].name);
		}
	}

	hide() {
		this.parent.practitioner_selector.hide();
	}

	show() {
		this.parent.practitioner_selector.show();
	}

	set_categories() {
		const categories = this.parent.selected_practitioner
			? this.practitioners
					.filter((f) => f.name === this.parent.selected_practitioner)
					.reduce((acc, val) => acc.concat(val.categories), [])
			: [];
		this.form.set_df_property("category", "options", categories);
		let hidden = categories.length ? 0 : 1;
		this.form.set_df_property("category", "hidden", hidden);

		const category_column = this.form.fields_list.find(
			(col) => col.df.fieldname === "category_column"
		);
		if (categories.length === 1) {
			this.form.set_value("category", categories[0]);
			$(category_column.wrapper).hide();
		} else {
			this.form.set_value("category", "");
			$(category_column.wrapper).show();
		}
	}

	set_appointment_types() {
		const appointment_types =
			this.parent.selected_practitioner &&
			this.parent.selected_appointment_category
				? this.practitioners
						.filter(
							(f) => f.name === this.parent.selected_practitioner
						)
						.reduce(
							(acc, val) => acc.concat(val.appointment_types),
							[]
						)[0]
						[this.parent.selected_appointment_category].reduce(
							(acc, val) => acc.concat(val.name),
							[]
						)
				: [];
		this.form.set_df_property(
			"appointment_type",
			"options",
			appointment_types
		);
		let hidden = appointment_types.length ? 0 : 1;
		this.form.set_df_property("appointment_type", "hidden", hidden);

		if (appointment_types.length === 1) {
			this.form.set_value("appointment_type", appointment_types[0]);
		} else {
			this.form.set_value("appointment_type", "");
		}
	}

	set_description() {
		const description = this.parent.selected_appointment_type
			? this.practitioners
					.filter((f) => f.name === this.parent.selected_practitioner)
					.reduce(
						(acc, val) => acc.concat(val.appointment_types),
						[]
					)[0]
					[this.parent.selected_appointment_category].filter(
						(f) => f.name === this.parent.selected_appointment_type
					)[0].description
			: null;

		const html = `<div class="alert bg-white border rounded">${
			description || ""
		}</div>`;
		let hidden = description ? 0 : 1;
		this.form.set_df_property("description", "hidden", hidden);
		!hidden && this.form.get_field("description").$wrapper.html(html);
	}
};

toobibpro.appointment.AppointmentCalendar = class AppointmentCalendar {
	constructor(opts) {
		Object.assign(this, opts);
		this.fullCalendar = null;
		this.slots = [];
		this.selected_slot = {};
		this.loading = false;
		this.render();
		toobibpro.appointment_update.on("update", () => {
			this.fullCalendar ? this.refresh() : this.render();
		});
	}

	render() {
		if (
			this.parent.selected_practitioner &&
			this.parent.selected_appointment_type
		) {
			const calendarEl = this.parent.event_selector;
			this.fullCalendar = new Calendar(
				calendarEl[0],
				this.calendar_options()
			);
			this.fullCalendar.render();
		}
	}

	get_initial_display_view() {
		return frappe.is_mobile() ? "dayGridDay" : "dayGridWeek";
	}

	set_initial_display_view() {
		this.fullCalendar.changeView(this.get_initial_display_view());
	}

	get_header_toolbar() {
		return {
			left: frappe.is_mobile() ? "today" : "dayGridWeek,dayGridDay",
			center: "prev,title,next",
			right: frappe.is_mobile() ? "" : "today",
		};
	}

	set_option(option, value) {
		this.fullCalendar.setOption(option, value);
	}

	destroy() {
		this.fullCalendar.destroy();
	}

	refresh() {
		this.fullCalendar.refetchEvents();
	}

	hide() {
		this.parent.event_selector.hide();
		this.parent.top_section.hide();
	}

	show() {
		this.parent.event_selector.show();
		this.parent.top_section.show();
	}

	calendar_options() {
		const me = this;
		return {
			eventClassNames: "event-slot-calendar",
			initialView: me.get_initial_display_view(),
			headerToolbar: me.get_header_toolbar(),
			weekends: me.parent.selected_practitioner
				? me.parent.practitioners.filter(
						(f) => f.name == me.parent.selected_practitioner
				  )[0].week_end
				: false,
			allDayContent: function () {
				return __("All Day");
			},
			buttonText: {
				today: __("Today"),
				month: __("Month"),
				week: __("Week"),
				day: __("Day"),
				list: __("Day"), // List of slots for each day
			},
			plugins: [
				timeGridPlugin,
				listPlugin,
				interactionPlugin,
				dayGridPlugin,
			],
			locale:
				frappe.get_cookie("preferred_language") ||
				frappe.boot.lang ||
				"en",
			initialDate: moment().add(1, "d").format("YYYY-MM-DD"),
			firstDay: frappe.datetime.get_first_day_of_the_week_index(),
			noEventsContent: __("No events to display"),
			events: function (info, callback) {
				return me.getAvailableSlots(info, callback);
			},
			selectAllow: this.getSelectAllow,
			validRange: this.getValidRange,
			defaultDate: this.getDefaultDate,
			eventClick: function (event) {
				me.eventClick(event);
			},
		};
	}

	getAvailableSlots(parameters, callback) {
		if (
			!this.parent.selected_practitioner ||
			!this.parent.selected_appointment_type
		)
			return [];
		frappe
			.call(this.slotAvailabilityMethod(), {
				start: moment(parameters.start).format("YYYY-MM-DD"),
				end: moment(parameters.end).format("YYYY-MM-DD"),
				practitioner: this.parent.selected_practitioner,
				appointment_type: this.parent.selected_appointment_type,
			})
			.then((result) => {
				this.slots = result.message || [];

				if (!this.slots.length) {
					this.loading = true;
					this.getNextAvailability(parameters.start);
				}

				callback(this.slots);
			});
	}

	getNextAvailability(start_dt) {
		if (
			start_dt &&
			this.parent.selected_practitioner &&
			this.parent.selected_appointment_type
		) {
			frappe
				.call({
					method: "toobibpro.templates.pages.appointment.get_next_availability",
					type: "GET",
					args: {
						practitioner: this.parent.selected_practitioner,
						start: moment(start_dt).format("YYYY-MM-DD"),
						appointment_type: this.parent.selected_appointment_type,
						is_group: Boolean(this.parent.is_group_appointment()),
					},
				})
				.then((r) => {
					if (r.message.status) {
						new toobibpro.appointment.NextDateSelector({
							parent: this,
							next_date: r.message.date,
							next_date_status: r.message.status,
						});
					}
				});
		}
	}

	eventClick(event) {
		this.selected_slot = event.event;
		new toobibpro.appointment.SlotConfirmation({ parent: this });
	}

	cancelEventSelection() {
		this.selected_slot = null;
		this.message = "";
	}

	slotAvailabilityMethod() {
		return this.parent.is_group_appointment()
			? "toobibpro.templates.pages.appointment.check_group_events_availabilities"
			: "toobibpro.templates.pages.appointment.check_availabilities";
	}

	submitEvent() {
		this.btn_disabled = true;
		frappe
			.call({
				method: "toobibpro.templates.pages.appointment.submit_appointment",
				args: {
					email: frappe.session.user,
					practitioner: this.parent.selected_practitioner,
					appointment_type: this.parent.selected_appointment_type,
					start: moment(this.selected_slot.start).format(
						"YYYY-MM-DD HH:mm:SS"
					),
					end: moment(this.selected_slot.end).format(
						"YYYY-MM-DD HH:mm:SS"
					),
					notes: this.message,
				},
			})
			.then((r) => {
				this.btn_disabled = false;
				this.$refs.fullCalendar.getApi().refetchEvents();
				this.cancelEventSelection();
				const message = r.message.appointment
					? __(
							"Thank you ! You will receive a confirmation message in a few minutes."
					  )
					: __(
							"An unexpected error prevented the submission of your request. Please contact your practitioner directly."
					  );
				const indicator = r.message.appointment ? "green" : "red";
				frappe.msgprint({ message: message, indicator: indicator });
			});
	}

	getSelectAllow(selectInfo) {
		return moment().diff(selectInfo.start) <= 0;
	}

	getValidRange() {
		return { start: moment().add(1, "d").format("YYYY-MM-DD") };
	}

	formatted_date(dt) {
		return momentjs(dt).locale("fr").format("LL");
	}
};

toobibpro.appointment.SlotConfirmation = class SlotConfirmation {
	constructor(opts) {
		Object.assign(this, opts);
		this.event = this.parent.selected_slot;
		this.btn_disabled = false;
		this.render();
	}

	render() {
		const html = `
			<div class="appointment-modal">
				<div class="appointment-modal-close">
					${frappe.utils.icon("close", "xl")}
				</div>
				<div class="appointment-modal-header">
					<h2 class="appointment-modal-date">${this.parent.formatted_date(
						this.event.start
					)}</h2>
					<h3 class="appointment-modal-time">${this.formatted_time(
						this.event.start
					)} - ${this.formatted_time(this.event.end)}</h3>
				</div>
				<div class="appointment-modal-header">
					<h2 class="appointment-modal-appointment-type">${
						this.parent.parent.selected_appointment_type
					}</h2>
					<h3 class="appointment-modal-seats-left hidden">${
						this.parent.seats_left || 0
					} ${
			this.parent.seats_left === 1
				? __("Remaining seat")
				: __("Remaining seats")
		}</h3>
				</div>
				<div class="form">
					<div class="form-box">
						<div class="form-fields">
							<textarea id="message" class="form-input" rows="3" name="message" placeholder="${__(
								"Your message"
							)}"></textarea>
						</div>
					</div>
					<button class="form-button">${__("Confirm")}</button>
				</div>
			</div>
		`;

		this.parent.parent.is_group_appointment() &&
			$(".appointment-modal-seats-left").removeClass("hidden");
		this.parent.hide();
		this.parent.parent.event_confirmation.html(html);
		this.bind_events();
	}

	bind_events() {
		$(".appointment-modal-close").on("click", (e) => {
			e.preventDefault();
			this.destroy();
			this.parent.show();
		});

		$(".form-button").on("click", (e) => {
			e.preventDefault();
			$(".form-button").text(__("Confirmation..."));
			$(".form-button").attr("disabled", true);
			this.submitEvent();
		});
	}

	submitEvent() {
		frappe
			.call({
				method: "toobibpro.templates.pages.appointment.submit_appointment",
				args: {
					email: frappe.session.user,
					practitioner: this.parent.parent.selected_practitioner,
					appointment_type:
						this.parent.parent.selected_appointment_type,
					start: moment(this.event.start).format(
						"YYYY-MM-DD HH:mm:SS"
					),
					end: moment(this.event.end).format("YYYY-MM-DD HH:mm:SS"),
					notes: $("#message").val(),
				},
			})
			.then((r) => {
				this.parent.refresh();
				this.parent.selected_slot = null;
				const message = r.message.appointment
					? __(
							"Thank you ! You will receive a confirmation message in a few minutes."
					  )
					: __(
							"An unexpected error prevented the submission of your request. Please contact your practitioner directly."
					  );
				const indicator = r.message.appointment ? "green" : "red";
				frappe.msgprint({ message: message, indicator: indicator });
				this.destroy();
				this.parent.show();
			});
	}

	destroy() {
		this.parent.parent.event_confirmation.html("");
	}

	formatted_time(dt) {
		return momentjs(dt).locale("fr").format("LT");
	}
};

toobibpro.appointment.NextDateSelector = class NextDateSelector {
	constructor(opts) {
		Object.assign(this, opts);
		this.render();
	}

	render() {
		// if (this.next_date_status !== 200) {
		// 	return frappe.msgprint({
		// 		message: __('No additional slots available for this appointment type'),
		// 		indicator: "red",
		// 	})
		// }
		const innerHtml =
			this.next_date_status === 200
				? `
			<div class="appointment-modal-header">
				<h2 class="appointment-modal-title">${__("The next available slot is on:")}</h2>
				<h2 class="appointment-modal-date">${this.parent.formatted_date(
					this.next_date
				)}</h2>
			</div>
			<button class="form-button">${__("See this date")}</button>
		`
				: `
			<div class="appointment-modal-close">
				${frappe.utils.icon("close", "xl")}
			</div>
			<div class="appointment-modal-header">
				<h2 class="appointment-modal-title pt-5">${__(
					"No additional slots available for this appointment type"
				)}</h2>
			</div>
		`;
		const html = `
			<div class="appointment-modal">
				${innerHtml}
			</div>
		`;
		this.parent.parent.event_selector.hide();
		this.parent.parent.next_date_selector.html(html);
		this.bind_events();
	}

	destroy() {
		this.parent.parent.next_date_selector.html("");
	}

	bind_events() {
		$(".appointment-modal-close").on("click", (e) => {
			e.preventDefault();
			this.destroy();
			this.parent.fullCalendar.gotoDate(
				moment().add(1, "d").format("YYYY-MM-DD")
			);
			this.parent.show();
		});

		$(".form-button").on("click", (e) => {
			e.preventDefault();
			this.goToNextDate();
		});
	}

	goToNextDate() {
		this.parent.fullCalendar.gotoDate(this.next_date);
		this.parent.refresh();
		this.destroy();
		this.parent.parent.event_selector.show();
	}
};
