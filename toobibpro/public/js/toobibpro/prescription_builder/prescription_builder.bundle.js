import { renderPrescriptionBuilder } from "./index";

frappe.provide("toobibpro");
frappe.provide("toobibpro.prescription_builder");
toobibpro.prescription_builder.renderPrescriptionBuilder = renderPrescriptionBuilder;
