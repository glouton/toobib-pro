// @ts-check
import { defineStore } from "pinia";

// const reqd = (v, msg) => { if (!v) throw new Error(msg); };

export const useStore = defineStore("prescription-builder", {
	state: () => ({
		/** @type {import("./utils").Row[]} */
		rows: [],
	}),

	actions: {
		/** @param {import("./utils").Row} row */
		addRow(row) {
			this.rows.push({
				...row,
				// "type": reqd(row.type, "missing row.type"),
			});
		},

		/** @param {import("./utils").Row} row */
		deleteRow(row) {
			const index = this.rows.indexOf(row);
			if (index >= 0) {
				this.rows.splice(index, 1);
			}
		}
	},
});
