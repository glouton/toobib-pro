// Copyright (c) 2016, DOKOS and contributors
// For license information, please see license.txt

frappe.ui.form.on("Professional Information Card", {
	onload: function(frm) {
		if (!frm.roles_editor && frm.doc.user) {
			const role_area = $('<div class="role-editor">').appendTo(
				frm.fields_dict.roles_html.wrapper
			);

			frappe.model.with_doc("User", frm.doc.user).then(() => {
				let user = frappe.model.get_doc("User", frm.doc.user);
				let user_frm = {
					doc: user,
					dirty: () => {
						frm.dirty()
						frm.selected_roles = frm.roles_editor.multicheck.get_checked_options()
					}
				}
				frm.roles_editor = new frappe.RoleEditor(
					role_area,
					user_frm,
					user_frm.doc.role_profile_name ? 1 : 0
				);
			})
		}
	},
	before_save: function(frm) {
		frappe.call({
			method: "register_roles",
			doc: frm.doc,
			args: {
				selected_roles: frm.selected_roles
			}
		})
	},
	refresh: function (frm) {
		if (!frm.is_new() && !frm.doc.user && frm.doc.email) {
			frappe.confirm(
				__("Do you want to create a user for this practitioner ?"),
				function () {
					frm.call("create_user");
					frappe.show_alert({
						message: __("User creation in progress"),
						indicator: "green",
					});
				}
			);
		}

		frappe.db.get_value(
			"Google Settings",
			"Google Settings",
			"enable",
			(r) => {
				if (r && r.enable === "1") {
					frm.toggle_display("google_calendar_section", true);
				}
			}
		);
	},
	sender_name: function (frm) {
		if (frm.doc.sender_name && !isAlphaNumeric(frm.doc.sender_name)) {
			frm.set_value("sender_name", null);
			frappe.throw(__("Please enter only alphanumeric values"));
		}
	},
});

const isAlphaNumeric = (ch) => {
	return ch.match(/^[a-z0-9]+$/i) !== null;
};
