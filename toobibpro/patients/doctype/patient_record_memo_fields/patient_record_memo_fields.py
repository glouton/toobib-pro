# Copyright (c) 2023, Association Toobib and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document


class PatientRecordMemoFields(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		display_condition: DF.Code | None
		field: DF.Autocomplete | None
		important_condition: DF.Code | None
		is_table: DF.Check
		label: DF.Data | None
		link_doctype: DF.Link | None
		parent: DF.Data
		parentfield: DF.Data
		parenttype: DF.Data
		table_fields: DF.SmallText | None
		type: DF.Literal["Section", "Data"]
	# end: auto-generated types
	pass
