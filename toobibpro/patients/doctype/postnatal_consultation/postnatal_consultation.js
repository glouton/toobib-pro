// Copyright (c) 2019, DOKOS and contributors
// For license information, please see license.txt

frappe.provide("toobibpro")

toobibpro.consultations_common.setup_consultations_controller()

frappe.ui.form.on("Postnatal Consultation", {
	onload(frm) {
		frm.fields_dict['pregnancy_folder'].get_query = function (doc) {
			return {
				filters: {
					"patient_record": doc.patient_record
				}
			}
		}
	}
})

extend_cscript(cur_frm.cscript, new toobibpro.BaseConsultationController({ frm: cur_frm }));
