// Copyright (c) 2017, DOKOS and contributors
// For license information, please see license.txt

frappe.provide('toobibpro');

toobibpro.consultations_common.setup_consultations_controller()

frappe.ui.form.on('Prenatal Interview Consultation', {
	onload(frm) {
		frm.fields_dict['prenatal_interview_folder'].get_query = function (doc) {
			return {
				filters: {
					"patient_record": doc.patient_record
				}
			}
		}
	}
});

toobibpro.PrenatalInterviewConsultationController = class PrenatalInterviewConsultationController extends toobibpro.BaseConsultationController {
};

extend_cscript(cur_frm.cscript, new toobibpro.PrenatalInterviewConsultationController({ frm: cur_frm }));
