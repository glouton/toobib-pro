// Copyright (c) 2017, DOKOS and contributors
// For license information, please see license.txt

frappe.ui.form.on("Prescription", {
	onload: function(frm) {
		if (frm.doc.docstatus != 1) {
			frappe.db.get_value("Professional Information Card", {user: frappe.session.user}, "name", (res) => {
				if (res?.name) {
					frm.set_value("practitioner", res.name);
				}
			});
		}
	},

	refresh(frm) {
		const htmlElement = frm.fields_dict.prescription_builder.$wrapper[0];
		htmlElement.innerHTML = "Loading...";

		frappe.require("prescription_builder.bundle.js", () => {
			let initialState = {};
			try {
				initialState = JSON.parse(frm.doc?.prescription_builder_state);
			} catch (e) {
				// pass
			}

			const persistState = (newState) => {
				// frm.set_value("prescription_builder_state", JSON.stringify(newState));
				console.log("persistState", newState);
			};
			toobibpro.prescription_builder.renderPrescriptionBuilder({ htmlElement, initialState, persistState });
		});
	},
});
