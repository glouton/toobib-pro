# Copyright (c) 2018, DOKOS and contributors
# For license information, please see license.txt

from typing import TYPE_CHECKING

import frappe
from frappe import _

from toobibpro.patients.doctype.patient_record.dashboard.memo import MemoContext, MemoRoot

if TYPE_CHECKING:
	from toobibpro.patients.doctype.patient_record_memo_fields.patient_record_memo_fields import (
		PatientRecordMemoFields,
	)


def get_patient_dashboard(patient_record):
	if frappe.db.exists("Custom Patient Record Dashboard", dict(patient_record=patient_record)):
		return frappe.get_doc("Custom Patient Record Dashboard", dict(patient_record=patient_record))

	else:
		dashboard = frappe.new_doc("Custom Patient Record Dashboard")
		dashboard.patient_record = patient_record
		try:
			dashboard.insert()
		except Exception:
			doc = frappe.get_doc("Custom Patient Record Dashboard", patient_record)
			frappe.rename_doc(
				doc.doctype,
				patient_record,
				doc.patient_record,
				force=True,
				merge=True if frappe.db.exists(doc.doctype, doc.patient_record) else False,
			)
			return doc

		return dashboard


@frappe.whitelist()
def get_memo(patient_record: str):
	if not isinstance(patient_record, str):
		return {"error": "Expected string: patient_record"}
	if not frappe.db.exists("Patient Record", patient_record):
		return {"error": "Patient Record not found", "patient_record": patient_record}

	try:
		practioner_settings = get_patient_dashboard(patient_record)
		dashboard_settings = frappe.get_single("Patient Record Memo")
		excluded_list = [
			(excluded_field.doc_type, excluded_field.fieldname)
			for excluded_field in practioner_settings.excluded_fields
		]

		def item_filter(raw_item: "PatientRecordMemoFields"):
			if (raw_item.link_doctype, raw_item.field) in excluded_list:
				return False
			return True

		context = MemoContext(
			query_filters={
				"Patient Record": {"name": patient_record},
				"*": {"patient_record": patient_record},
			},
			item_filter=item_filter,
		)

		root = MemoRoot.FromFields(
			dashboard_settings.fields,
			context=context,
			label=patient_record or _("Memo"),
		)
		return root.as_dict()

	except Exception:
		print(frappe.get_traceback())
		raise
