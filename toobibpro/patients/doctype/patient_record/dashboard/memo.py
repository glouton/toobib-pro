import re
from dataclasses import dataclass
from typing import TYPE_CHECKING

import frappe
from frappe import _

from .memo_item import (
	MemoItem,
	MemoItem_Section,
	MemoSource_Docfield,
	MemoSource_Error,
	MemoSource_Table,
)

if TYPE_CHECKING:
	from frappe.model.meta import Meta

	from .memo_item import MemoField


@dataclass
class MemoRoot(MemoItem):
	children: list[MemoItem]
	label: str = ""
	type: str = "memo"

	@classmethod
	def FromFields(cls, fields: list["MemoField"], *, context: "MemoContext", **kwargs):
		return cls(
			**kwargs,
			children=make_memo_items(build_tree(fields), context),
		)


DEFAULT_SECTION: "MemoField" = frappe._dict({"label": "", "type": "Section"})


def build_tree(raw_items: "list[MemoField]"):
	tree = []
	last = frappe._dict({"section": None})

	def create_new_section(item):
		last.section = item
		item._subitems = []
		tree.append(item)

	def add_to_last_section(item):
		if not last.section:
			create_new_section(DEFAULT_SECTION)
		last.section._subitems.append(item)

	for item in raw_items:
		item_type = getattr(item, "type", None)
		if item_type in ("Section",):
			create_new_section(item)
		else:
			add_to_last_section(item)

	return tree


class MemoUtils:
	@classmethod
	def parse_table_field_descriptor(cls, s: str):
		# Comma or Blank separated list of fields
		if not isinstance(s, str):
			return []
		out = []
		for part in re.split(r"(,|\s)", s):
			part = part.strip()
			if part:
				out.append(part)
		return out

	@classmethod
	def meta_has_field(cls, meta: "Meta", fieldname: str):
		return meta.has_field(fieldname) or fieldname in meta.default_fields

	@classmethod
	def _check_error(cls, meta: "Meta", fieldname: str, filters: dict):
		if not cls.meta_has_field(meta, fieldname):
			return f"Invalid field: {fieldname}"

		for key in filters or []:
			if not cls.meta_has_field(meta, key):
				return f"Invalid filters: {frappe.as_json(filters)}"


@dataclass
class MemoContext:
	query_filters: dict[str, dict]
	item_filter: "callable[[MemoField], bool]"

	def get_query_filters_for_item(self, raw_item: "MemoField"):
		# Order of specificity: doctype:fieldname, doctype, fieldname, *
		return (
			self.query_filters.get(raw_item.link_doctype + ":" + raw_item.field)
			or self.query_filters.get(raw_item.link_doctype)
			or self.query_filters.get(raw_item.field)
			or self.query_filters.get("*")
			or {}
		)

	def build_source(self, raw_item: "MemoField"):
		if raw_item.is_table:
			return self._build_source_table(raw_item)

		if not raw_item.link_doctype:
			return MemoSource_Error("Not implemented")

		return self._build_source_single(raw_item)

	def _build_source_single(self, raw_item: "MemoField"):
		meta = frappe.get_meta(raw_item.link_doctype)
		filters = self.get_query_filters_for_item(raw_item)

		if err := MemoUtils._check_error(meta, raw_item.field, filters):
			return MemoSource_Error(err)

		return MemoSource_Docfield(
			doctype=raw_item.link_doctype, filters=filters, fieldname=raw_item.field, context=self
		)

	def _build_source_table(self, raw_item: "MemoField"):
		meta = frappe.get_meta(raw_item.link_doctype)
		filters = self.get_query_filters_for_item(raw_item)

		if err := MemoUtils._check_error(meta, raw_item.field, filters):
			return MemoSource_Error(err)

		return MemoSource_Table(
			doctype=raw_item.link_doctype,
			filters=filters,
			fieldname=raw_item.field,
			columns=MemoUtils.parse_table_field_descriptor(raw_item.table_fields),
		)

	def eval(self, condition: str, eval_locals: dict, default_value: bool = False):
		if not condition or not isinstance(condition, str):
			return default_value
		if condition.startswith("eval:"):
			condition = condition[5:]
		return frappe.safe_eval(condition, eval_locals=eval_locals)


def make_memo_items(raw_items: "list[MemoField]", context: MemoContext):
	output = []
	for raw_item in raw_items:
		item = make_memo_item(raw_item, context)
		if item:
			output.append(item)
	return output


def make_memo_item(raw_item: "MemoField", context: MemoContext):
	if not context.item_filter(raw_item):
		return None

	if raw_item.type == "Section":
		return MemoItem_Section(
			label=_(raw_item.label),
			children=make_memo_items(raw_item._subitems, context),
		)
	elif raw_item.type == "Data":
		source = context.build_source(raw_item)
		item = source.to_item(raw_item)
		return item
