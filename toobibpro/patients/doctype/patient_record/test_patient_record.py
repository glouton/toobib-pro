# Copyright (c) 2017, DOKOS and Contributors
# See license.txt


import unittest

import frappe


class TestPatientRecord(unittest.TestCase):
	def test_patient_record(self):
		patient = create_patient_record()

		self.assertEqual(patient.patient_first_name, "_Test")
		self.assertEqual(patient.patient_last_name, "Patient")

		# TODO


def create_patient_record():
	patient = frappe.get_doc(
		{
			"doctype": "Patient Record",
			"patient_first_name": "_Test",
			"patient_last_name": "Patient",
		}
	)

	patient.insert()

	return patient
