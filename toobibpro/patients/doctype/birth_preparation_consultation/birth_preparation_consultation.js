// Copyright (c) 2017, DOKOS and contributors
// For license information, please see license.txt

frappe.provide('toobibpro');

toobibpro.consultations_common.setup_consultations_controller()

frappe.ui.form.on("Birth Preparation Consultation", {
	onload(frm) {
		frm.fields_dict['pregnancy_folder'].get_query = function (doc) {
			return {
				filters: {
					"patient_record": doc.patient_record
				}
			}
		}
		frm.fields_dict['prenatal_interview_folder'].get_query = function (doc) {
			return {
				filters: {
					"patient_record": doc.patient_record
				}
			}
		}
	}
});

cur_frm.cscript = new toobibpro.BaseConsultationController({ frm: cur_frm });
